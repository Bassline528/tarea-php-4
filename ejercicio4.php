<?php
    // Realizar el ejercicio 3, pero ahora utilizando PDO - PHP Data Objects

    $db_connection =  new PDO('pgsql:host=127.0.0.1;dbname=ejercicio1;','tarea4_user','admin');

    $query = "SELECT p.nombre as p_nombre, 
    p.precio as p_precio, 
    m.nombre as m_nombre, 
    e.nombre as e_nombre, 
    c.nombre as c_nombre 
    FROM producto p, marca m, empresa e, categoria c 
    WHERE p.id_marca = m.id_marca
    and m.id_empresa = e.id_empresa
    and c.id_categoria = p.id_categoria";

    $res = $db_connection->prepare($query);

    $res->execute();

    $res = $res->fetchAll( PDO::FETCH_OBJ );

    

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Ejercicio4</title>
</head>
<body>
    <table>
        <tr>
			<td> Nom. Producto </td>
			<td> Precio </td>
			<td> Marca </td>
			<td> Empresa </td>
			<td> Categoria </td>
        </tr>
        <?php

        foreach ($res as $element) { ?>

            <tr>
                <td><?php echo $element->p_nombre; ?></td>
                <td><?php echo $element->p_precio; ?></td>
                <td><?php echo $element->m_nombre; ?></td>
                <td><?php echo $element->e_nombre; ?></td>
                <td><?php echo $element->c_nombre; ?></td>

            </tr>
        <?php  } ?>

    </table>
</body>
</html>

