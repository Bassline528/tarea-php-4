<?php
// • Crear una BD en postgreSQL llamada ejercicio4 (usar el pgAdmin)
// • Crear una tabla con tres campos:
// o id: campo de tipo serial y que sea PK
// o nombre: un campo que sea una cadena de 40 caracteres
// o Descripción: un campo que sea un varchar de 250 caracteres
// • Hacer un script PHP que inserte 1000 registros en la tabla (los valores deben ser aleatorios
// y deben ser generados automáticamente con el script).
// • Hacer un script PHP que visualice esos 1000 registros insertados previamente. La visualización se deberá hacer de manera tabular

$db_connect = pg_connect('host=127.0.0.1 port=5432 dbname=ejercicio4 user=tarea4_user password=admin') or die('No se ha podido conectar: ' . pg_last_error());;

$sql = "";
for ($i = 1; $i < 1000; $i++) {
    $nombre         = "Nombre " . $i;
    $descripcion    = "Descripcion " . $i;
    $query          = "INSERT INTO TABLE1 (nombre, descripcion) VALUES ('$nombre','$descripcion');";
    $sql           .= $query;
}

$result = pg_query($sql);
if (!$result) {
    "Ocurrio un error en el query";
    exit;
}

			 
$result = pg_query("SELECT * FROM TABLE1") or die('La consulta fallo: ' . pg_last_error());


echo "<table>";

while ($line = pg_fetch_array($result, null, PGSQL_ASSOC)) {
    echo "<tr>";
    foreach ($line as $element) {
        echo "<td>$element</td>";
    }
    echo "</tr>";
}


echo "</table>";


