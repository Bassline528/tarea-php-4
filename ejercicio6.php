<?php
session_start();
if (isset($_POST['login'])) {
  $db_connect = pg_connect('host=127.0.0.1 port=5432 dbname=tarea4_db user=tarea4_user password=admin') or die('No se ha podido conectar: ' . pg_last_error());;

    $username = $_POST['username'];
    $password = $_POST['password'];

    if($username && $password) {

      $res = pg_query($db_connect, "SELECT * FROM users WHERE username='$username'");

      if(!$res) {
        echo "<h5>Ocurrio un error en el query</h5>";
        exit;
      }

      $user = pg_fetch_object($res);

      if(password_verify($password, $user->pass)){
        echo "<p>Auntenticado correctamente!</p>";
      } else {
        echo "<p>Algo salio mal!</p>";
      }

      pg_close($db_connect);
      
    }


}

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.3.1/dist/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <title>Ejercicio6</title>
</head>
<body>
<section class="vh-100" style="background-color: #508bfc;">
<form action="" method="post" name="login">
    <div class="container py-5 h-100">
      <div class="row d-flex justify-content-center align-items-center h-100">
        <div class="col-12 col-md-8 col-lg-6 col-xl-5">
          <div class="card shadow-2-strong" style="border-radius: 1rem;">
            <div class="card-body p-5 text-center">
  
              <h3 class="mb-5">Sign in</h3>
              
  
              <div class="form-outline mb-4">
                <input type="text" name="username" id="typeEmailX-2" class="form-control form-control-lg" />
                <label class="form-label" for="typeEmailX-2">Username</label>
              </div>
  
              <div class="form-outline mb-4">
                <input type="password" name="password" id="typePasswordX-2" class="form-control form-control-lg" />
                <label class="form-label" for="typePasswordX-2">Password</label>
              </div>
  
          
  
              <button class="btn btn-primary btn-lg btn-block" type="submit" name="login" value="login">Login</button>
  
              <hr class="my-4">
  
              
  
            </div>
          </div>
        </div>
      </div>
    </div>
</form>
</section>
    
</body>
</html>