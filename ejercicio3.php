<?php
// Hacer un script en PHP que se conecte a la BD, anteriormente creada en el “ejercicio1” y
// haga lo siguiente:
// Realizar una consulta a dicha BD para que traiga los siguientes datos (respetar
// orden):nombre_producto,precio_producto,nombre_marca,nombre_empresa,nombre_categoría
// Presentar los datos de manera tabular (usando tablas HTML generadas a partir de
// PHP)
// Aclaración: Usar la extensión de pgsql


    $db_connect = pg_connect('host=127.0.0.1 port=5432 dbname=ejercicio1 user=tarea4_user password=admin');
    if(!$db_connect) {
        echo "<h1>Ocurrio un error en la conexion </h1>";
        exit;
    }

    $res = pg_query($db_connect, 
        "SELECT p.nombre, p.precio, m.nombre, e.nombre, c.nombre 
        FROM producto p, marca m, empresa e, categoria c 
        WHERE p.id_marca = m.id_marca
        and m.id_empresa = e.id_empresa
        and c.id_categoria = p.id_categoria"
    );

    if(!$res) {
        echo "<h1>Ocurrio un error en el query</h1>";
        exit;
    }


    echo "<table>";
		echo "<tr>";
			echo "<td> Nom. Producto </td>";
			echo "<td> Precio </td>";
			echo "<td> Marca </td>";
			echo "<td> Empresa </td>";
			echo "<td> Categoria </td>";
		echo "</tr>";

	while ($row = pg_fetch_row($res))
	{
		echo "<tr>";
			echo "<td> $row[0] </td>";
			echo "<td> $row[1] </td>";
			echo "<td> $row[2] </td>";
			echo "<td> $row[3] </td>";
			echo "<td> $row[4] </td>";
		echo "</tr>";
	}

	echo "</table>";

	pg_close($db_connect);
?>

